set nu
set nowrap
set showmatch 
set hlsearch
set incsearch
set ignorecase
syntax on
set showcmd
set hlsearch
set ruler
set visualbell
set mouse=a
set spell
set guiheadroom=0
" let g:molokai_original = 1
set nocompatible              " be iMproved, required

filetype off                  " required
filetype plugin on

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'dracula/vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
call vundle#end()            " required
filetype plugin indent on    " required

colorscheme dracula
